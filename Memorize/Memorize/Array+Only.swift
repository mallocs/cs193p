//
//  Array+Only.swift
//  Memorize
//
//  Created by mallocs.net on 7/19/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import Foundation

extension Array  {
    var only: Element? {
        count == 1 ? first : nil
    }
}
