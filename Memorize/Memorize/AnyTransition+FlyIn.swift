//
//  AnyTransition+FlyIn.swift
//  Memorize
//
//  Created by mallocs.net on 8/6/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

extension AnyTransition {
    static var flyIn: AnyTransition {
        let insertion = AnyTransition.move(edge: .trailing)
            .combined(with: .opacity)
        let removal = AnyTransition.scale
            .combined(with: .opacity)
        return .asymmetric(insertion: insertion, removal: removal)
    }
}
