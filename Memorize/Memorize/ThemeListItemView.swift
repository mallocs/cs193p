//
//  ThemeListItemView.swift
//  Memorize
//
//  Created by mallocs.net on 9/9/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

struct ThemeListItemView: View {
    @EnvironmentObject var store: ThemeStore
    var theme: Theme
    let themeIndex: Int
    var isEditing: Bool = false
    @State private var showThemeEditor = false

    var body: some View {
        HStack {
            if isEditing {
                Image(systemName: "pencil.circle.fill").imageScale(.large).foregroundColor(theme.color)
                .onTapGesture {
                    self.showThemeEditor = true
                }
                .popover(isPresented: $showThemeEditor) {
                    ThemeEditor(isShowing: self.$showThemeEditor, chosenThemeIndex: self.themeIndex)
                        .environmentObject(self.store)
                        .frame(minWidth: 300, minHeight: 500)
                }
            }
            VStack(alignment: .leading) {
                Text(theme.title).foregroundColor(theme.color).font(.headline)
                Text(
                    (theme.pairsOfCards == theme.emojis.count ? "All of " : "\(theme.pairsOfCards) pairs from ")
                        + "\(theme.emojis)")
                    .font(.caption)
            }
        }
    }
}


struct ThemeEditor: View {
    @EnvironmentObject var store: ThemeStore
    
    @Binding var isShowing: Bool
    var chosenThemeIndex: Int
    var chosenTheme : Theme {
        get {
            self.store.themes[chosenThemeIndex]
        }
    }

    @State private var emojisToAdd: String = ""
    @State private var themeName: String = ""
    
    var body: some View {
        VStack(spacing: 0) {
            ZStack {
                Text("\(self.themeName)").font(.headline).padding()
                HStack {
                    Spacer()
                    Button(action: {
                        self.isShowing = false
                    }, label: { Text("Done") }).padding()
                }
            }
            Divider()
            Form {
                Section {
                    TextField("Theme Name", text: $themeName, onEditingChanged: { began in
                        if !began {
                            self.themeName = self.store.renameTheme(at: self.chosenThemeIndex, to: self.themeName)
                        }
                    })
                }
                Section(header: Text("Add Emoji").fontWeight(.bold)) {
                    TextField("Emoji", text: $emojisToAdd, onEditingChanged: { began in
                        if !began {
                            self.store.addEmoji(self.emojisToAdd, toEmojis: self.chosenTheme.emojis, at: self.chosenThemeIndex)
                            self.emojisToAdd = ""
                        }
                    })
                }
                Section(header: HStack {
                    Text("Remove Emoji").fontWeight(.bold)
                    Spacer()
                    Text("tap emojis to exclude").font(.caption)
                }) {
                    Grid(self.chosenTheme.emojis.map { String($0) }, id: \.self) { emoji in
                        Text(emoji).font(Font.system(size: self.fontSize))
                            .onTapGesture {
                                self.store.removeEmoji(emoji, fromEmojis: self.chosenTheme.emojis, at: self.chosenThemeIndex)
                        }
                    }
                    .frame(height: self.height)
                }
                Section(header: Text("Card Count").fontWeight(.bold)) {
                    HStack {
                        Stepper(onIncrement: {
                            self.store.incrementPairsOfCards(at: self.chosenThemeIndex)
                        }, onDecrement: {
                            self.store.decrementPairsOfCards(at: self.chosenThemeIndex)
                        }, label: { Text("\(self.chosenTheme.pairsOfCards) Pairs") })

                    }
                }
                Section(header: Text("Color").fontWeight(.bold)) {
                    Grid(self.store.colors.map { UIColor($0) }, id: \.self) { color in
                        ZStack {
                            RoundedRectangle(cornerRadius: self.cornerRadius)
                                .fill(Color(color))
                                .padding(self.colorPadding)
                            if Color(color) == self.chosenTheme.color {
                                Image(systemName: "checkmark.circle").foregroundColor(.white).offset(x: self.checkmarkOffset.x, y: self.checkmarkOffset.y)
                            }
                                
                        }.onTapGesture {
                                self.store.changeThemeColor(to: color.rgb, at: self.chosenThemeIndex)
                        }
                    }
                    .frame(height: self.colorPickerHeight)
                }
            }
        }
        .onAppear {
            self.themeName = self.store.themes[self.chosenThemeIndex].title
        }
    }

    // MARK: - Drawing Constants
    private var height: CGFloat {
        CGFloat((chosenTheme.emojis.count - 1) / 6) * 70 + 70
    }
    private var colorPickerHeight: CGFloat {
        CGFloat((store.colors.count - 1) / 6) * 70 + 70
    }

    private let cornerRadius: CGFloat = 10
    private let fontSize: CGFloat = 40
    private let colorPadding: CGFloat = 10
    private let checkmarkOffset = CGPoint(x: 13, y: 13)
}
