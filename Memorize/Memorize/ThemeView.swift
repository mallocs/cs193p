//
//  ThemeView.swift
//  Memorize
//
//  Created by Vegas Dot Com on 9/9/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

struct ThemeListItemView: View {
    let theme: MemoryGameTheme
    var body: some View {
        Text(theme.title).font(.headline)
    }
}

struct ThemeView_Previews: PreviewProvider {
    static var previews: some View {
        ThemeListItemView(theme: defaultTheme)
    }
}
