//
//  EmojiMemoryGameView.swift
//  Memorize
//
//  Created by mallocs.net on 7/9/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    @ObservedObject var viewModel: EmojiMemoryGame
    var body: some View {
        VStack(spacing: 5) {
            HStack(spacing: 5) {
                Text(self.viewModel.currentTheme.title).padding(.horizontal, 5)
                Spacer()
                Text("Score: \(self.viewModel.score)").padding(.horizontal, 5)
            }
            Grid(viewModel.cards) { card in
                    CardView(card: card).onTapGesture {
                        withAnimation(.linear(duration: 0.75)) {
                            self.viewModel.choose(card: card)
                        }
                    }
            .padding(5)
            }
            .foregroundColor(self.viewModel.currentTheme.color)
            Button(action: {
                withAnimation(.easeInOut) {
                    self.viewModel.reset()
                }
            }) {
                Text("New Game")
                    .padding(10)
                    .foregroundColor(.white)
                    .background(self.viewModel.currentTheme.color)
                    .cornerRadius(10)
            }
        .padding(5)
        }
    .padding(15)
    }
}

struct CardView: View {
    let card: MemoryGame<String>.Card

    var body: some View {
        GeometryReader { geometry in
            self.body(for: geometry.size)
        }
    }
    
    @State private var animatedBonusRemaining: Double = 0
    
    private func startBonusTimeAnimation() {
        animatedBonusRemaining = card.bonusRemaining
        withAnimation(.linear(duration: card.bonusTimeRemaining)) {
            animatedBonusRemaining = 0
        }
    }
    @ViewBuilder
    private func body(for size: CGSize) -> some View {
        if card.isFaceUp || !card.isMatched {
             ZStack {
                Group {
                    if card.isConsumingBonusTime {
                        Pie(startAngle: Angle.degrees(0 - 90), endAngle: Angle.degrees(-animatedBonusRemaining*360 - 90), clockwise: true)
                            .onAppear {
                                self.startBonusTimeAnimation()
                            }
                    } else {
                        Pie(startAngle: Angle.degrees(0 - 90), endAngle: Angle.degrees(-card.bonusRemaining*360 - 90), clockwise: true)
                    }
                }
                .padding(5).opacity(0.4)
                .transition(.identity)
                Text(card.content)
                    .font(Font.system(size: fontSize(for: size)))
                    .rotationEffect(Angle.degrees(card.isMatched ? 360 : 0))
                    .animation(card.isMatched ? Animation.linear(duration: 1).repeatForever(autoreverses: false) : .default)
            }
            .cardify(isFaceUp: card.isFaceUp)
            .transition(AnyTransition.scale)
        }
    }
    // MARK: - Drawing Constants
    private func fontSize(for size: CGSize) -> CGFloat {
        min(size.width, size.height) * 0.68
    }
}

































struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let store = ThemeStore(named: "Memory Game")
        let game = EmojiMemoryGame(withTheme: store.themes[0])
        game.choose(card: game.cards[2])
        return EmojiMemoryGameView(viewModel: game)
    }
}

