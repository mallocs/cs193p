//
//  ThemeChooser.swift
//  Memorize
//
//  Created by mallocs.net on 9/8/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

struct ThemeChooser: View {
    @EnvironmentObject var store: ThemeStore
    @State private var editMode: EditMode = .inactive

    var body: some View {
        NavigationView {
            List {
                ForEach(Array(store.themes.enumerated()), id: \.offset) {
                    index, theme in
                    NavigationLink(destination: EmojiMemoryGameView(viewModel: EmojiMemoryGame(withTheme: theme))) {
                        ThemeListItemView(theme: theme, themeIndex: index, isEditing: self.editMode.isEditing)

                    }
                }
                .onDelete { indexSet in
                    indexSet.forEach { self.store.themes.remove(at: $0) }
                }
            }
            .navigationBarTitle("\(self.store.name)", displayMode: .inline)
            .navigationBarItems(
                leading: Button(action: {
                    self.store.themes.append(self.store.makeRandomTheme())
                }, label: {
                    Image(systemName: "plus").imageScale(.large)
                }),
                trailing: EditButton()
            )
            .environment(\.editMode, $editMode)
        }
    }
}

struct ThemeChooser_Previews: PreviewProvider {
    static var previews: some View {
        ThemeChooser()
    }
}
