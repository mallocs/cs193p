//
//  EmojiMemoryGame.swift
//  Memorize
//
//  Created by mallocs.net on 7/14/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

class EmojiMemoryGame: ObservableObject {
    @Published private var model: MemoryGame<String>

    private (set) var currentTheme: Theme

    init(withTheme theme: Theme) {
        self.currentTheme = theme
        self.model = EmojiMemoryGame.createMemoryGame(withTheme: theme)
    }

    private static func createMemoryGame(withTheme theme: Theme) -> MemoryGame<String> {
        let emojis = theme.emojis.map { String($0) }
        return MemoryGame<String>(numberOfPairsOfCards: theme.pairsOfCards) { pairIndex in emojis[pairIndex] }
    }

    // MARK: - Access to cards
    var cards: Array<MemoryGame<String>.Card> {
        model.cards
    }
    
    // MARK: - Access to score
    var score: Int {
        get {
            model.score
        }
    }
    
    // MARK: - Intents
    func choose(card: MemoryGame<String>.Card) {
        objectWillChange.send()
        model.choose(card: card)
    }
    
    func reset() {
  // TODO: Make random reset     self.currentTheme = store.themes.randomElement()!  // .randomElement()!.value
        self.model = EmojiMemoryGame.createMemoryGame(withTheme: self.currentTheme)
    }
}
