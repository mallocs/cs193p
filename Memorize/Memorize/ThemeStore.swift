//
//  ThemeStore.swift
//  Memorize
//
//  Created by mallocs.net on 9/8/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI
import Combine

struct Theme : Codable, Identifiable {
    let id = UUID()
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    var emojis: String
    var pairsOfCards: Int
    var title: String
    var rgb: UIColor.RGB
    var color: Color {
        get {
            return Color(UIColor(self.rgb))
        }
    }
}

class ThemeStore: ObservableObject
{
    let name : String
    private var autosave: AnyCancellable?
    @Published var themes : [Theme] = []

    init(named name: String = "Themes") {
        self.name = name
        let defaultsKey = "ThemeStore"
        if let data = UserDefaults.standard.data(forKey: defaultsKey) {
            if let decoded = try? JSONDecoder().decode([Theme].self, from: data) {
                self.themes = decoded
            }
        } else {
            self.themes = [makeRandomTheme()]
        }
        autosave = $themes.sink { themes in
            let encoded = try? JSONEncoder().encode(themes)
            UserDefaults.standard.set(encoded, forKey: defaultsKey)
        }
    }
    
    func makeRandomTheme() -> Theme {
        let emojiCount = Int.random(in: minEmojiPairCount...maxEmojiPairCount)
        let pairsOfCards = min(emojiCount, Int.random(in: minEmojiPairCount...maxEmojiPairCount))
        let randomColorIndex = Int.random(in: 0..<colors.count)
        return Theme(
            emojis: String.randomEmojis(length: emojiCount),
            pairsOfCards: pairsOfCards,
            title: "Random Emojis",
            rgb: UIColor(colors[randomColorIndex]).rgb
        )
    }

    // MARK: - Intents
    @discardableResult
    func changeThemeColor(to rgb: UIColor.RGB, at index: Int) -> Color {
        themes[index].rgb = rgb
        return themes[index].color
    }
    
    @discardableResult
    func incrementPairsOfCards(at index: Int) -> Int {
        if (themes[index].pairsOfCards < themes[index].emojis.count && themes[index].pairsOfCards < maxEmojiPairCount) {
            themes[index].pairsOfCards += 1
        }
        return themes[index].pairsOfCards
    }

    @discardableResult
    func decrementPairsOfCards(at index: Int) -> Int {
        if (themes[index].pairsOfCards > minEmojiPairCount) {
            themes[index].pairsOfCards -= 1
        }
        return themes[index].pairsOfCards
    }

    @discardableResult
    func renameTheme(at index: Int, to newTitle: String) -> String {
        themes[index].title = newTitle
        return themes[index].title
    }
    
    @discardableResult
    func addEmoji(_ emoji: String, toEmojis emojis: String, at index: Int) -> String {
        return changeThemeEmojis(index, to: (emoji + emojis).uniqued())
    }
    
    @discardableResult
    func removeEmoji(_ emojisToRemove: String, fromEmojis emojis: String, at index: Int) -> String {
        return changeThemeEmojis(index, to: emojis.filter { !emojisToRemove.contains($0) })
    }

    private func changeThemeEmojis(_ index: Int, to newEmojis: String) -> String {
        themes[index].pairsOfCards = min(newEmojis.count, themes[index].pairsOfCards)
        themes[index].emojis = newEmojis
        return newEmojis
    }
    // MARK: - Constants
    private let minEmojiPairCount = 4
    private let maxEmojiPairCount = 12
    let colors = [UIColor.systemRed.rgb,
                  UIColor.systemBlue.rgb,
                  UIColor.systemGray.rgb,
                  UIColor.systemTeal.rgb,
                  UIColor.systemOrange.rgb,
                  UIColor.systemYellow.rgb,
                  UIColor.systemGreen.rgb,
                  UIColor.systemIndigo.rgb,
                  UIColor.systemPurple.rgb,
                  UIColor.black.rgb]
}
