//
//  String+RandomEmoji.swift
//  Memorize
//
//  Created by mallocs.net on 9/13/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import Foundation

extension String{
    static var randomEmoji: String {
        let range = [UInt32](0x1F601...0x1F64F)
        let ascii = range[Int(drand48() * (Double(range.count)))]
        let emoji = UnicodeScalar(ascii)?.description
        return emoji!
    }
    static func randomEmojis(length: Int) -> String {
        var randomString: String = ""

        for _ in 0..<length {
            randomString += randomEmoji
        }
        return randomString
    }
}
