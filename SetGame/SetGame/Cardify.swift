//
//  Cardify.swift
//  SetGame
//
//  Created by Vegas Dot Com on 8/6/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

struct Cardify: ViewModifier {
    var isFaceUp: Bool
    var isSelected: Bool
    var isMatched: Bool?

    var borderColor: Color {
        if isMatched != nil {
            return isMatched! ? Color.green : Color.red
        }
        return isSelected ? Color.blue : Color.black
    }

    func body(content: Content) -> some View {
        ZStack {
            Group {
                RoundedRectangle(cornerRadius: cornerRadius).fill(Color.white)
                RoundedRectangle(cornerRadius: cornerRadius).strokeBorder(borderColor, lineWidth: edgeLineWidth)
                content
            }.animation(nil)
                .opacity(isFaceUp ? 1 : 0)
            RoundedRectangle(cornerRadius: cornerRadius).fill()
                .opacity(isFaceUp ? 0 : 1)
        }
    }
    private let cornerRadius: CGFloat = 10
    private let edgeLineWidth: CGFloat = 3
}

extension View {
    func cardify(isFaceUp: Bool, isSelected: Bool, isMatched: Bool?) -> some View {
        self.modifier(Cardify(isFaceUp: isFaceUp, isSelected: isSelected, isMatched: isMatched))
    }
}
