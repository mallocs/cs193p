//
//  SoloSetGame.swift
//  SetGame
//
//  Created by Vegas Dot Com on 7/29/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

class SoloSetGame: ObservableObject {
    @Published private var model: SetGame?
    var activeCards: Array<SetGame.SetCard> {
        return Array(self.dealtCards.filter{ card in !(card.isMatched ?? false) || card.isSelected })
    }
    init() {
        self.model = SoloSetGame.createSoloSetGame()
    }
    private static func createSoloSetGame() -> SetGame {
        return SetGame()
    }

    // MARK: Acess to cards
    var dealtCards: Array<SetGame.SetCard> {
        model?.dealtCards ?? Array<SetGame.SetCard>()
    }

    var undealtCards: Array<SetGame.SetCard> {
        model?.cards ?? Array<SetGame.SetCard>()
    }

    // MARK: Intents
    func choose(card: SetGame.SetCard) {
        objectWillChange.send()
        model?.choose(card: card)
    }
    func reset() {
        objectWillChange.send()
        self.model = nil
        self.model = SoloSetGame.createSoloSetGame()
    }
    func deal(count: Int) {
        objectWillChange.send()
        model?.deal(count: count)
    }
}
