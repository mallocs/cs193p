//
//  MemoryGame.swift
//  Memorize
//
//  Created by Vegas Dot Com on 7/14/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import Foundation

struct SetGame {
    private (set) var cards: Array<SetCard>
    private (set) var dealtCards = Array<SetCard>()
    private let matchCount = 3
    
    var selectedCardsAreMatching: Bool {
        get {
            let selectedCardsIndexes = getSelectedCardsIndexes()
            if (selectedCardsIndexes.count == matchCount && dealtCards[selectedCardsIndexes[0]].isMatched == true) {
                return true
            }
            return false
        }
    }
    
    static func areMatching(cards: Array<SetCard>) -> Bool {
        let match1 = SetCard.compare(cards[0], cards[1])
        let match2 = SetCard.compare(cards[1], cards[2])
        let match3 = SetCard.compare(cards[0], cards[2])
        return match1 == match2 && match1 == match3
    }
    
    func getSelectedCardsIndexes() -> Array<Int> {
        var selectedCardsIndexes = Array<Int>()
        for index in 0..<dealtCards.count {
            if (dealtCards[index].isSelected == true) {
                selectedCardsIndexes.append(index)
            }
         }
        return selectedCardsIndexes
    }

    mutating func activateMatchStatus() {
        let selectedCardsIndexes = getSelectedCardsIndexes()
        var matchStatus = false
        if (selectedCardsIndexes.count == matchCount) {
            var selectedCardsArray = Array<SetCard>()
            for index in 0..<selectedCardsIndexes.count {
                selectedCardsArray.append(dealtCards[selectedCardsIndexes[index]])
            }
            matchStatus = SetGame.areMatching(cards: selectedCardsArray)
        }
        for index in selectedCardsIndexes {
            dealtCards[index].isMatched = matchStatus
         }
    }
    mutating func resetSelectedCards(at indexes: Array<Int>) {
        for index in indexes {
            dealtCards[index].isSelected = false
            dealtCards[index].isMatched = dealtCards[index].isMatched == false ? nil : dealtCards[index].isMatched
        }
    }

    mutating func choose(card: SetCard) {
        if let chosenIndex = dealtCards.firstIndex(matching: card) {
            let selectedCardsIndexes = getSelectedCardsIndexes()
            if (selectedCardsIndexes.count == 3) {
                resetSelectedCards(at: selectedCardsIndexes)
            }
            if !(self.dealtCards[chosenIndex].isMatched ?? false) {
                self.dealtCards[chosenIndex].isSelected = !self.dealtCards[chosenIndex].isSelected
            }
            if (self.dealtCards[chosenIndex].isSelected && selectedCardsIndexes.count == 2) {
                activateMatchStatus()
            }
        }
    }
    
    mutating func deal(count: Int) {
        guard self.cards.count - count >= 0 else {
            return
        }
        let range = 0..<count
        self.dealtCards += Array(cards[range])
        self.cards.removeSubrange(range)
    }
    
    static func generateCards() -> Array<SetCard> {
        var cards = Array<SetCard>()
        var index = 0
        for count in Count.allCases {
            for shape in CardShape.allCases {
                for shade in Shading.allCases {
                    for color in CardColor.allCases {
                        cards.append(SetCard(count: count, shape: shape, shading: shade, color: color, id: index))
                        index += 1;
                    }
                }
            }
        }
        return cards
    }

    init() {
        cards = SetGame.generateCards()
        cards.shuffle()
    }

    struct SetCard: Equatable, Identifiable {
        let count: Count
        let shape: CardShape
        let shading: Shading
        let color: CardColor
        var isSelected: Bool = false
        var isMatched: Bool?
        var isFaceUp: Bool = true
        var id: Int
        static func compare(_ lhs: SetCard, _ rhs: SetCard) -> [String: Bool] {
            return ["count": lhs.count == rhs.count, "shape": lhs.shape == rhs.shape, "shading": lhs.shading == rhs.shading, "color": lhs.color == rhs.color]
        }
    }
}

protocol CardFeature: CaseIterable, Equatable {}

enum Count: Int, CaseIterable, Equatable, CardFeature {
    case one, two, three
}
enum CardShape: Int, CaseIterable, Equatable {
    case squiggle, oval, diamond
}
enum Shading: Int, CaseIterable, Equatable {
    case solid, outline, stripped
}
enum CardColor: Int, CaseIterable, Equatable {
    case red, green, blue
}
