//
//  Pie.swift
//  Memorize
//
//  Created by Vegas Dot Com on 7/23/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

struct Diamond: Shape, InsettableShape {
    var insetAmount: CGFloat = 0
    var squeezeFactor: CGFloat = 0.7
    func inset(by amount: CGFloat) -> some InsettableShape {
        var shape = self
        shape.insetAmount += amount
        return shape
    }

    func path(in rect: CGRect) -> Path {
        let center = CGPoint(x: rect.midX, y: rect.midY)
        let height = min(rect.width, rect.height)/2
        let p1 = CGPoint(x: center.x, y: center.y + height)
        let p2 = CGPoint(x: center.x + height*squeezeFactor, y: center.y)
        let p3 = CGPoint(x: center.x, y: center.y - height)
        let p4 = CGPoint(x: center.x - height*squeezeFactor, y: center.y)

        var p = Path()
        p.move(to: p1)
        p.addLine(to: p2)
        p.addLine(to: p3)
        p.addLine(to: p4)
        p.addLine(to: p1)
        return p
    }
}
