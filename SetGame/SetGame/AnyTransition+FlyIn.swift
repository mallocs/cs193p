//
//  AnyTransition+FlyIn.swift
//  Memorize
//
//  Created by Vegas Dot Com on 8/6/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

extension AnyTransition {
    static func flyIn(for size: CGSize, factor: Double) -> AnyTransition {
        let point = generateRandomOffsetPoint(for: size, factor: factor)
        let insertion = AnyTransition.offset(point)
        let removal = AnyTransition.offset(point)
        return .asymmetric(insertion: insertion, removal: removal)
    }
}

private func generateRandomOffsetPoint(for size: CGSize, factor: Double) -> CGSize {
    let randomPositiveX = Bool.random() ? 1.0 : -1.0
    let randomPositiveY = Bool.random() ? 1.0 : -1.0
    return CGSize(
        width: Double(size.width) * factor * Double(size.width/size.height) * Double.random(in: 1..<2) * randomPositiveX,
        height: Double(size.height) * factor * Double(size.width/size.height) * Double.random(in: 1..<2) * randomPositiveY
    )
}
