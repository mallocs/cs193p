//
//  SoloSetGameView.swift
//  SetGame
//
//  Created by Vegas Dot Com on 7/29/20.
//  Copyright © 2020 mallocs.net. All rights reserved.
//

import SwiftUI

struct SoloSetGameView: View {
    @ObservedObject var viewModel: SoloSetGame

    var body: some View {
        GeometryReader { geometry in
            self.body(for: geometry.size)
        }
    }

    private func body(for size: CGSize) -> some View {
        VStack(spacing: 10) {
            HStack(spacing: 5) {
                Text("Solo Set").padding(.horizontal, 5)
            }
            Grid(viewModel.activeCards) { card in
                CardView(card: card).onTapGesture {
                    withAnimation(.linear(duration: 1.4)) {
                        self.viewModel.choose(card: card)
                    }
                }
                .transition(AnyTransition.flyIn(for: size, factor: 2.0))
                .padding(5)
            }
            .onAppear {
                withAnimation(.linear(duration: 1.4)) {
                    self.viewModel.deal(count: 12)
                }
            }
            HStack(spacing: 35) {
                Button(action: {
                    withAnimation(.linear(duration: 0.2)) {
                        self.viewModel.reset()
                    }
                    withAnimation(Animation.linear(duration: 1.4).delay(1)) {
                        self.viewModel.deal(count: 12)
                    }
                }) {
                    Text("New Game")
                        .padding(5)
                        .foregroundColor(.white)
                        .background(Color.gray)
                        .cornerRadius(8)
                }
                if self.viewModel.undealtCards.count > 0 {
                    Button(action: {
                        withAnimation(.linear(duration: 1.4)) {
                            self.viewModel.deal(count: min(self.viewModel.undealtCards.count, 3))
                        }
                    }) {
                        Text("Deal \(min(self.viewModel.undealtCards.count, 3)) Cards")
                            .padding(5)
                            .foregroundColor(.white)
                            .background(Color.gray)
                            .cornerRadius(8)
                    }
                } else {
                    Text("No Cards Left")
                    .padding(5)
                    .foregroundColor(.white)
                    .background(Color.gray)
                    .cornerRadius(8)
                }
            }
        }
    .padding(10)
    }
}

struct CardView: View {
    private (set) var card: SetGame.SetCard
    
    var body: some View {
        GeometryReader { geometry in
            self.body(for: geometry.size)
        }
    }

    @ViewBuilder
    private func body(for size: CGSize) -> some View {
        ZStack {
            VStack(spacing: 0) {
                ForEach(1..<(card.count.rawValue+2)) { count in
                    ShapeView(shape: self.card.shape, shading: self.card.shading, color: self.card.color)
                        .padding(8.0 - CGFloat(Double(self.card.count.rawValue)))
                }
            }
            .cardify(isFaceUp: self.card.isFaceUp, isSelected: self.card.isSelected, isMatched: self.card.isMatched) // .animation(nil)
        }
    }

    // MARK: - Drawing Constants
    private func fontSize(for size: CGSize) -> CGFloat {
        min(size.width, size.height) * 0.68
    }
}

struct ShapeView: View {
    let shape: CardShape
    let shading: Shading
    let color: CardColor
    
    private var _color: Color {
        switch self.color {
            case .red: return Color.red
            case .green: return Color.green
            case .blue: return Color.blue
        }
    }
    private var border: Bool {
        switch self.shading {
            case .solid: return false
            case .outline: return true
            case .stripped: return true
        }
    }
    private var opacity: Double {
        switch self.shading {
            case .solid: return 1.0
            case .outline: return 0.0
            case .stripped: return 0.5
        }
    }
    
    var body: some View {
        GeometryReader { geometry in
            self.body(for: geometry.size)
        }
    }

    private func body(for size: CGSize) -> some View {
        ZStack {
            if (shape == .oval) {
                Capsule()
                    .fill(_color)
                    .aspectRatio(2, contentMode: .fit)
                    .opacity(opacity)
                Capsule()
                    .strokeBorder(_color, lineWidth: border ? 2 : 0)
                    .aspectRatio(2, contentMode: .fit)
            } else if (shape == .squiggle) {
                Rectangle()
                    .fill(_color)
                    .aspectRatio(2, contentMode: .fit)
                    .opacity(opacity)
                Rectangle()
                    .strokeBorder(_color, lineWidth: border ? 2 : 0)
                    .aspectRatio(2, contentMode: .fit)
            } else if (shape == .diamond) {
                Diamond()
                    .fill(_color)
                    .opacity(opacity)
                Diamond()
                    .strokeBorder(_color, lineWidth: border ? 2 : 0)
            }
        }
    }
}
